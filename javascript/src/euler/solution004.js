// https://projecteuler.net/problem=4

// A palindromic number reads the same both ways.
// The largest palindrome made from the product of two
// 2-digit numbers is 9009 = 91 x 99
// Find the largest palindrome made from the product of two 3-digit numbers.

// Answer: 906609

Object.defineProperty(Number.prototype, "palindromeTest", {
  value: function () {
    var stringy = '' + this;

    for (var x = 0 , y = stringy.length - 1 ; x < y ; x++, y--) {
      if (stringy[x] !== stringy[y]) {
        return false;
      }
    }

    return true;
  },
  writable: true,
  configurable: true
});

module.exports = {
  answer : () => {
    var max_pali = 0;
    for(i = 100; i < 999; i++) {
      for(j = 100; j < 999; j++) {
        var t = (i * j);
        if( t > max_pali && t.palindromeTest() ) {
          max_pali = t;
        }
      }
    }
    return max_pali;
  }
}

