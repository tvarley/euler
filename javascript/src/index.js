var solution001 = require('../src/euler/solution001');
var solution002 = require('../src/euler/solution002');
var solution003 = require('../src/euler/solution003');
var solution004 = require('../src/euler/solution004');
var solution005 = require('../src/euler/solution005');
var solution006 = require('../src/euler/solution006');

module.exports = () => {
  console.log(`Solution 001: ${solution001.answer()}`);
  console.log(`Solution 002: ${solution002.answer()}`);
  console.log(`Solution 003: ${solution003.answer()}`);
  console.log(`Solution 004: ${solution004.answer()}`);
  console.log(`Solution 005: ${solution005.answer()}`);
  console.log(`Solution 006: ${solution006.answer()}`);
}
